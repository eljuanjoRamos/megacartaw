import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//ROUTES
import { app_routing } from './app.routes';

//SERVICES
import { UsuarioService } from './services/usuario.service';
import { TypeService } from './services/types/type.service';
import { StoreTypeService } from './services/types/storetype.service';
import { StoreService } from './services/store/store.service';
import { ProductService } from './services/store/product.service';
import { CategoryStoreService } from './services/store/categorystore.service';
import { AuthGuardService } from './services/auth-guard.service';
///Verdaderos Componentes
import { AppComponent } from './app.component';
import { SignupComponent } from './components/signup/signup.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { StoreTypeComponent} from './components/storetype/storetype.component';
import { StoreComponent } from './components/store/store.component';
import { DashboardComponent} from './components/dashboard/dashboard.component';
import { DashboardAdminComponent} from './components/dashboard/dashboard-admin.component';
import { NavbarComponent } from './components/dashboard/navbar/navbar.component';
import { MyNavbarComponent } from './components/dashboard/navbar/mynavbar.component';
import { MyNavbar2Component } from './components/dashboard/navbar/mynavbar2.component';
import { AddProductComponent } from './components/add-component/addProduct.component';
//imoprt { AddCategoryComponent } from './components/add-component/addCategory.component'; 
@NgModule({
  declarations: [
    HomeComponent,
    StoreTypeComponent,
    StoreComponent,
    AppComponent,
    LoginComponent,
    SignupComponent,
    DashboardComponent,
    DashboardAdminComponent,
    NavbarComponent,
    MyNavbarComponent,
    MyNavbar2Component,
    AddProductComponent
    //AddCategoryComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    app_routing
  ],
  providers: [
    UsuarioService,
    AuthGuardService,
    TypeService,
    StoreTypeService,
    StoreService,
    ProductService,
    CategoryStoreService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
