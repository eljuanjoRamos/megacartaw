import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class UsuarioService {
  usuarios:any[];
  constructor(
    private http:Http,
    private router:Router
  ) {}
  public getHeader() {
    let  headers = new Headers({
      'Authorization': localStorage.getItem('token')
    });
    return headers;
  }
  
  public editarUsuario(data:any) {
    let uri = "http://localhost:3000/api/v1/usuario/" + "editar";
    let datos = JSON.stringify(data);
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token')
    });
    return this.http.put(uri, datos, { headers })
    .map(res => {
      return res.json();
    });
  }
  public eliminarCuenta(confirmar:boolean) {
    let uri = "http://localhost:3000/api/v1/usuario/" + "editar";
    let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': localStorage.getItem('token')
    });
    return this.http.delete(uri, { headers })
    .map(res => {
      return res.json();
    });
  }  


  public autenticar(usuario:any) {
    let uriUsuario:string = "http://localhost:3000/authenticate/";
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    
    let options = new RequestOptions({headers : headers});
    let data = JSON.stringify(usuario);
    console.log(data);
    this.http.post(uriUsuario, data, options)
    .subscribe(res => {
      let token = res.json().token;
      let rol = res.json().idRol;
      if(token) {
        console.log("Si existe el token");
        localStorage.setItem('token', token);
        localStorage.setItem('id', res.json().idUsuario);
        if(rol == 1) {
          this.router.navigate(['/dashboard/usuario']);
        } else {
          this.router.navigate(['/dashboard/admin']);
        }
      } else {
        console.log("No existen token");
        return false;
      }
    }, error => {
      console.log(error.text());
    })

  }

   public registrar(usuario:any) {
    let uriUsuario:string = "http://localhost:3000/api/v1/usuario";
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    let options = new RequestOptions({headers : headers});
    let data = JSON.stringify(usuario);

    return this.http.post(uriUsuario, data, options)
    .map(res => {
      return res.json();
    }, error => {
      console.log(error.text());
    })

  }

  public verificarUsuario():boolean {
    if(localStorage.getItem('token')) {
      return true;
    }
    return false;
  }
  public getUsuarios() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZFVzdWFyaW8iOjEsIm5pY2siOiJASkh1ZXJ0YXMiLCJjb250cmFzZW5hIjoiMTIzNCIsImlhdCI6MTQ5OTk2MjkzNCwiZXhwIjoxNDk5OTY2NTM0fQ.V8elxq5UReuFkFRoA-AQJK-tEpOnqmpi7AREglHRsjI');
    let options = new RequestOptions({headers: headers});

    return this.http.get("http://localhost:3000/api/v1/usuario",)
    .map(res => {
      this.usuarios = res.json();
      console.log(this.usuarios);
    });
  }
}
