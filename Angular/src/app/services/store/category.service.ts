import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

var uri = "http://localhost:3000/api/ws/mc/category/bystore/";
var uri2 = "http://localhost:3000/api/ws/mc/category"
@Injectable()
export class CategoryService {
  constructor(
    private http:Http
  ) {}
 
  public getCategory() {
    return this.http.get(uri2)
    .map(res => res.json()
    );
  }

  public getCategoryByStore(id:any) {
    return this.http.get(uri+ id)
    .map(res => {
      return res.json();
    });
  }
  
  

public newCategory(c:any) {
    let data = JSON.stringify(c);
    return this.http.post(uri2, data)
    .map(res => {
      return res.json();
    });
  }

  public editCategory(c:any, id:any) {
    
    let data = JSON.stringify(c);
    
    return this.http.put(uri2 + id, data)
    .map(res => {
      return res.json();
    });
  }

  public deleteCategory(id:any) {
    return this.http.delete(uri2+id)
    .map(res => {
      return res.json();
    });
  }



}  