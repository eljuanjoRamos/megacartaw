import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

var uri = "http://localhost:3000/api/ws/mc/categorystore/";
var uri2 = "http://localhost:3000/api/ws/mc/product/bycategory/"
@Injectable()
export class CategoryStoreService {
  constructor(
    private http:Http
  ) {}
 
  public getCategoryStores() {
    return this.http.get(uri)
    .map(res => res.json()
    );
  }

  public getCategoryStore(id:any) {
    return this.http.get(uri+ id)
    .map(res => {
      return res.json();
    });
  }

  public getProductStore(id:any, id2:any) {
    return this.http.get(uri2+ id +"/"+id2)
    .map(res => {
      return res.json();
    });
  }
}  