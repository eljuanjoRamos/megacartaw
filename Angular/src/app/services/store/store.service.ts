import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

var uri = "http://localhost:3000/api/ws/mc/store/";
var uri2 = "http://localhost:3000/api/ws/mc/idstore/";

@Injectable()
export class StoreService {
  constructor(
    private http:Http
  ) {}
 
  public getStores() {
    return this.http.get(uri)
    .map(res => res.json()
    );
  }

  public getStore(id:any) {
    return this.http.get(uri+ id)
    .map(res => {
      return res.json();
    });
  }

  public getIdStore(id:any) {
    return this.http.get(uri2+ id)
    .map(res => {
      return res.json();
    });
  }

}  