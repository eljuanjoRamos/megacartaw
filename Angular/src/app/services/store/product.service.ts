import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

var uri = "http://localhost:3000/api/ws/mc/product/bystore/";
var uri2 = "http://localhost:3000/api/ws/mc/product/bycategory/"
var uri3 = "http://localhost:3000/api/ws/mc/product/"
@Injectable()
export class ProductService {
  constructor(
    private http:Http
  ) {}
 
  public getProducts() {
    return this.http.get(uri)
    .map(res => res.json()
    );
  }

  public getProductoByStore(id:any) {
    return this.http.get(uri+ id)
    .map(res => {
      return res.json();
    });
  }
  public getProductoByCategory(id:any) {
    return this.http.get(uri2+ id)
    .map(res => {
      return res.json();
    });
  }
  public getProduct(id:any) {
    console.log(id);
    return this.http.get(uri3+ id)
    .map(res => {
      return res.json();
      
    });
  }

public newProduct(product:any) {
    return this.http.post(uri3, product)
    .map(res => {
      return res.json();
    });
  }

  public editProduct(product:any, id:any) {
    return this.http.put(uri3 + id, product)
    .map(res => {
      console.log(res.json());
      return res.json();
    });
  }

  public deleteProduct(id:any) {
    return this.http.delete(uri3+id)
    .map(res => {
      return res.json();
    });
  }



}  