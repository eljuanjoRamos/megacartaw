import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

var uri = "http://localhost:3000/api/ws/mc/storetype/";
@Injectable()
export class StoreTypeService {
  constructor(
    private http:Http
  ) {}
 
  public getStoreTypes() {
    return this.http.get(uri)
    .map(res => res.json()
    );
  }

  public getStoreType(id:any) {
    return this.http.get(uri+ id)
    .map(res => {
      console.log(JSON.stringify(res.json()));
      return res.json();
    });
  }
}  