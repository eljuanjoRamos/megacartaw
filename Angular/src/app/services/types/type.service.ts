import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

var uri = "http://localhost:3000/api/ws/mc/type/";
@Injectable()
export class TypeService {
  constructor(
    private http:Http
  ) {}
 
  public getTypes() {
    return this.http.get(uri)
    .map(res => res.json()
    );
  }

  public getType(id:number) {
    return this.http.get(uri+ id)
    .map(res => {
      console.log(res.json());
      return res.json();
    });
  }

  

}
