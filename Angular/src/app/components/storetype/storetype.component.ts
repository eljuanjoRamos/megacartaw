import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StoreTypeService } from '../../services/types/storetype.service';

@Component({
  selector: 'app-storetype',
  templateUrl: 'storetype.component.html',
})
export class StoreTypeComponent implements OnInit {
  formularioCategoria:FormGroup;
  uri:string;
  storeType:any[] = [];
  notificacion:any = {
    estado: false,
    mensaje: ""
  }

  constructor(
    private storetypeService:StoreTypeService,
    private router:Router,
    private activatedRoute:ActivatedRoute
  ) {

    this.activatedRoute.params.subscribe(params => {
      this.uri = params["storetype"];
      console.log(params["storetype"]);
        this.storetypeService.getStoreType(params["storetype"]).subscribe(data => {
          this.storeType = data;
        });
    });
  }
  ngOnInit() {
  }
}
