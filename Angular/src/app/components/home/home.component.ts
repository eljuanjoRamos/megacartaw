import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TypeService } from '../../services/types/type.service';
import { StoreService } from '../../services/store/store.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {
  
  constructor(
    private typeService:TypeService,
    private storeService:StoreService,
    private router:Router
  ) {}
  type:any[] = [];
  store:any[] = [];
  private initializeType() {
    this.typeService.getTypes().subscribe(data => {
      this.type = data;
      console.log(data);
    })
  }
  private initializeStore() {
    this.storeService.getStores().subscribe(data => {
      this.store = data;
      console.log(data);
    })
  }
  
  ngOnInit() {
   this.initializeType();
   this.initializeStore(); 
  }

  

}