import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StoreService } from '../../services/store/store.service';
import { ProductService } from '../../services/store/product.service';
import { CategoryStoreService } from '../../services/store/categorystore.service';
@Component({
  selector: 'app-store',
  templateUrl: 'store.component.html',
})
export class StoreComponent implements OnInit {
  store:any[] = [];
  product:any[] = [];
  categoryStore:any[] = [];
  uri:string;
  constructor(
    private storeService:StoreService,
    private productService:ProductService,
    private categoryStoreService: CategoryStoreService,
    private router:Router,
    private activatedRoute:ActivatedRoute
  ) {
     this.activatedRoute.params.subscribe(params => {
      this.uri = params["idStore"];
        this.storeService.getStore(this.uri).subscribe(data => {
          this.store = data;
        });
        this.productService.getProductoByStore(this.uri).subscribe(data1 =>{
          this.product = data1;
        });
        this.categoryStoreService.getCategoryStore(this.uri).subscribe(data2 => {
          this.categoryStore = data2;
        });
        
    });
  }
  getId(id:any){
    this.categoryStoreService.getProductStore(id, this.uri).subscribe(data4 => {
        this.product = data4;
      });
  }
  getTodos(){
    this.productService.getProductoByStore(this.uri).subscribe(data1 =>{
          this.product = data1;
    });
  }
  ngOnInit() {
    
  }
  getIdProduct(id:any){
    console.log("el id del producto es " + id);
    //mandar al metodo agregar al arrito
  }
}
