import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProductService } from '../../services/store/product.service';
import { CategoryStoreService } from '../../services/store/categorystore.service';
@Component({
  selector: 'app-addProduct',
  templateUrl: 'addProduct.component.html',
})
export class AddProductComponent implements OnInit {
  formularioProducto:FormGroup;
  uri:string;
  product:any;
  idStore:any = localStorage.getItem('idStore');
  categoryStore:any[] = [];
  notificacion:any = {
    estado: false,
    mensaje: ""
  }
  
  constructor(
    private productService:ProductService,
    private router:Router, private service:CategoryStoreService,
    private activatedRoute:ActivatedRoute
   
  ) {
     this.service.getCategoryStore(this.idStore).subscribe(data2 => {
          this.categoryStore = data2;
        });
    let validaciones = [
      Validators.required, Validators.minLength(3)
    ];

    this.activatedRoute.params.subscribe(params => {
      this.uri = params["idProducto"];
      if(this.uri !== "nuevo") {
        this.productService.getProduct(this.uri)
        .subscribe(c => {
          this.product = c;
          this.formularioProducto = new FormGroup({
            'name': new FormControl(this.product[0].name, validaciones),
            'idCategory': new FormControl(this.product[0].idCategory, validaciones),
            'price': new FormControl(this.product[0].price, validaciones),
            'quantity': new FormControl(this.product[0].quantity, validaciones),
            'idStore': new FormControl(this.idStore, validaciones),
            'image': new FormControl(this.product[0].image, validaciones),
            'observations': new FormControl(this.product[0].observations, validaciones)
          });
        });
      } else {
        this.formularioProducto = new FormGroup({
          'name': new FormControl('', validaciones),
            'idCategory': new FormControl('', validaciones),
            'price': new FormControl('', validaciones),
            'quantity': new FormControl('', validaciones),
            'idStore': new FormControl(this.idStore, validaciones),
             'image': new FormControl('', validaciones),
            'observations': new FormControl('', validaciones)
        });
      }
    });
  }


  ngOnInit() {
    this.productService.getProducts().subscribe(data => {
        this.product = data;
      })
  }
  public guardar() {
    if(this.uri === "nuevo") {
      this.productService.newProduct(this.formularioProducto.value)
      .subscribe(res => {
            this.router.navigate(['/dashboard/admin']);
      });
      
    } else {
      console.log("Modificacion de contacto");
      console.log(this.formularioProducto.value);
      this.productService.editProduct(this.formularioProducto.value, this.uri)

      .subscribe(res => {
        this.router.navigate(['/dashboard/admin']); 
      });
    }
  }
}
