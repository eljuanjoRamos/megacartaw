import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { DashboardAdminComponent} from './dashboard-admin.component';
import { AddProductComponent } from '../add-component/addProduct.component';

export const dashboard_routes: Routes = [
    { path: 'usuario', component: DashboardComponent },
    { path: 'admin', component: DashboardAdminComponent },
    { path: 'producto/:idProducto', component: AddProductComponent },
    { path: '**', pathMatch: 'full', redirectTo: 'usuario' }
];
