import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mynavbar2',
  templateUrl: './mynavbar2.component.html',
})

export class MyNavbar2Component {

  constructor(
    private router:Router) { }

  salir() {
    localStorage.removeItem('token');
    this.router.navigate(["/home"]);
  }


}
