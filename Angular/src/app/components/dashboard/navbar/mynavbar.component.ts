import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-mynavbar',
  templateUrl: './mynavbar.component.html',
})

export class MyNavbarComponent {

  constructor(
    private router:Router) { }

  salir() {
    localStorage.removeItem('token');
    this.router.navigate(["/home"]);
  }


}
