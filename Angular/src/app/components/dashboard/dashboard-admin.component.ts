import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { StoreService } from '../../services/store/store.service';
import { ProductService } from '../../services/store/product.service';
import { CategoryStoreService } from '../../services/store/categorystore.service';
@Component({
  selector: 'app-dashboard-admin',
  templateUrl: './dashboard-admin.component.html',
})
export class DashboardAdminComponent implements OnInit {
  public idUsuario:any = localStorage.getItem('id');
  public idTienda:any
  public t:any = localStorage.getItem('idStore');
  product:any[] = [];
  categoryStore:any[] = [];
  constructor(
    private productService:ProductService,
    private storeService:StoreService,private service:CategoryStoreService,
    private router:Router
  ) {  }

  inicializar() {
    this.storeService.getIdStore(this.idUsuario).subscribe(data => {
      for (var xs = 0; xs < data.length; xs++) {
        this.idTienda = data[xs].idStore;
        localStorage.setItem('idStore', data[xs].idStore);
         this.service.getCategoryStore(data[xs].idStore).subscribe(data2 => {
          this.categoryStore = data2;
        });
    }

    });
  }
  inicializarProducto() {
    this.productService.getProductoByStore(localStorage.getItem('idStore')).subscribe(
          data =>{
            this.product = data;
      }) 
  }
  ngOnInit() {
   this.inicializar();
   this.inicializarProducto();
  }

  borrar(id:any) {
    this.productService.deleteProduct(id)
    .subscribe(res => {
        this.inicializarProducto();
    });
  }

}
