import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TypeService } from '../../services/types/type.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit {
  constructor(
    private typeService:TypeService,
    private router:Router
  ) { 
    
   }

  type:any[] = [];
  private initializeType() {
    this.typeService.getTypes().subscribe(data => {
      this.type = data;
    })
  }
  ngOnInit() {
   this.initializeType(); 
  }



}
