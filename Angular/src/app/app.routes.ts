import { RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from './services/auth-guard.service';

import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { SignupComponent } from './components/signup/signup.component';
import { dashboard_routes } from './components/dashboard/dashboard.routes';
import { StoreTypeComponent } from './components/storetype/storetype.component';
import { StoreComponent } from './components/store/store.component';
import { AddProductComponent } from './components/add-component/addProduct.component';

const APP_ROUTES:Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'producto/:idProducto', component: AddProductComponent },
  { path: 'storetype/:storetype', component: StoreTypeComponent },
  { path: 'store/:idStore', component: StoreComponent },
  {
    path: 'dashboard',
    children: dashboard_routes,
    canActivate: [ AuthGuardService ]
  },
  { path: '**', pathMatch: 'full', redirectTo:'home'}
]

export const app_routing = RouterModule.forRoot(APP_ROUTES);
