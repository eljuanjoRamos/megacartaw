var express = require('express');
var jwt = require('jsonwebtoken');
var user = require('../model/usuario.model');
var router = express.Router();

router.post('/authenticate/', function(req, res) {
	var data = {
		nick: req.body.nick,
		pass: req.body.pass
	}
	user.login(data, function(results) {
		if(results != 0) {
			var token = 'Bearer ' + jwt.sign(results[0], 'shh', { expiresIn: '1h' });
			results[0].estado = true;
			results[0].mensaje = "Se autorizo el acceso";
			results[0].token = token;
			res.json(results[0]);
		} else {
			res.json({
				estado: false,
				mensaje: "Nick o Contraseña incorrecto/a"
			});
		}
	});
});

module.exports = router;
