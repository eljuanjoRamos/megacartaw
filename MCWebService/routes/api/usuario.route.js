var express = require('express');
var usuario = require('../../model/usuario.model');
var usuarioRouter = express.Router();
var services = require('../../services');

usuarioRouter.get('/api/ws/mc/usuario/', function(req, res) {
  usuario.selectAll(function(resultado) {
    if(typeof resultado !== undefined) {
      res.json(resultado);
    } else {
      res.json({"mensaje" : "No hay usuarios"});
    }
  });
});

usuarioRouter.get('/usuario/historial', function(req, res) {
  usuario.selectHistorial(1, function(err, resultado) {
    if(typeof resultado !== undefined) {
      res.json(resultado);
    } else {
      res.json({"mensaje" : "No hay historial"});
    }
  });
});
usuarioRouter.post('/usuario', function(req, res) {
  var data = {
    nick : req.body.nick,
    contrasena: req.body.contrasena
  }
  usuario.insert(data, function(resultado) {
    if(typeof resultado !== undefined && resultado.affectedRows > 0) {
      res.json({
        estado : true,
        mensaje : "Se registo el usuario correctamente"
      });
    } else {
      resultado.status = false;
      resultado.mensaje = "Error no se registro el usuario";
      res.json(resultado);
    }
  });
});

usuarioRouter.put('/usuario/:idUsuario', services.verificar, function(req, res, next){  
	var data = {
		idUsuario: req.usuario.idUsuario,
    nick : req.body.nick,
    contrasena: req.body.contrasena
	}
  usuario.update(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "Se ha modificado con exito"
      });
    } else {
      res.json({
        estado: false,
        mensaje: "No se pudo modificar"
      });
    }
  });

});

usuarioRouter.delete('/usuario/:idUsuario', services.verificar,  function(req, res, next){
	var idUsuario = req.usuario.idUsuario;
  usuario.delete(idUsuario, function(resultado){
		if(resultado && resultado.mensaje ===	"Eliminado") {
			res.json({estado: true, mensaje:"Se elimino la usuario correctamente"});
		} else {
			res.json({estado:false, mensaje:"Se elimino la usuario"});
		}
	});
});

module.exports = usuarioRouter;
