var express = require('express');
var storetypeModel = require('../../model/storetype.model');
var storetypeRoute = express.Router();

storetypeRoute.route('/api/ws/mc/storetype/')
  .get(function(req, res) {
      storetypeModel.getAll(function(results) {
        if (typeof results !== undefined) {
          res.json(results);
        } else {
          res.json({
            status: false,
            "Message" : "There are no stores type",
          });
        }
      });
    })
  .post(function (req, res) {
    var data = {
        idStoreType: null,
        idStore: req.body.idStore,
        idType: req.body.idType
    }
    storetypeModel.insert(data, function (error, results) {
      if(results && results.status) {
        res.json({
            status: true,
            "Message": "There storetype has been inserted successfully",
          });
      } else {
        res.json({
            status: true,
            "Message": "There storetype hasn't been inserted successfully",
          });
      }
    });
  });

  storetypeRoute.route('/api/ws/mc/storetype/:idType/')
    .get(function(req, res) {
      var idType = req.params.idType;
      storetypeModel.getOne(idType, function(results) {
        if (typeof results !== undefined) {
          res.json(results);
        } else {
          res.json({
            status: false,
            "Message" : "The type your are searching does not exist",
          });
        }
      });
    })
    .delete(function (req, res) {
      var idType = req.params.idType;
      storetypeModel.delete(idType, function (error, results) {
        if (results && results.status) {
          res.json({
            status: true,
            "Message": "The storetype has been deleted successfully"
          });
        } else {
          res.json({
            status: false,
            "Message": "The storetype hasn't been deleted successfully"
          });
        }
      });
    })
    .put(function (req, res) {
      var idType = req.params.idType;
      var data = {
        idStore: req.body.idStore,
        idType: req.body.idType,
        idStoreType: req.body.idStoreType
      }
      if(idType === data.idStoreType) {
        storetypeModel.update(data, function(error, results) {
          if(results) {
            res.json({
              status: true,
              "Message": "The storetype has been updated successfully"
            });
          } else {
            res.json({
              status: false,
              "Message": "The storetype hasn't been updated successfully"
            });
          }
        });
      } else {
        res.json({
              status: false,
              "Message": "The storetype's id doesn't match"
            });
      }
    });

  module.exports = storetypeRoute;
