var express = require('express');
var categorystoreModel = require('../../model/categorystore.model');
var categorystoreRouter = express.Router();

categorystoreRouter.route('/api/ws/mc/categorystore/')
  .get(function(req, res) {
      categorystoreModel.getAll(function(results) {
        if (typeof results !== undefined) {
          res.json(results);
        } else {
          res.json({
            status: false,
            "Message" : "There are no categories store",
          });
        }
      });
    })
  .post(function (req, res) {
    var data = {
        idCateogry: req.body.idCateogry,
        idStore: req.body.idStore
    }
    categorystoreModel.insert(data, function (error, results) {
      if(results && results.status) {
        res.json({
            status: true,
            "Message": "There categorystore has been inserted successfully",
          });
      } else {
        res.json({
            status: true,
            "Message": "There categorystore hasn't been inserted successfully",
          });
      }
    });
  });

  categorystoreRouter.route('/api/ws/mc/categorystore/:idCategoryStore/')
    .get(function(req, res) {
      var idCategoryStore = req.params.idCategoryStore;
      categorystoreModel.getOneByStore(idCategoryStore, function(results) {
        if (typeof results !== undefined) {
          res.json(results);
        } else {
          res.json({
            status: false,
            "Message" : "The categories store your are searching does not exist",
          });
        }
      });
    })
    .delete(function (req, res) {
      var idCategoryStore = req.params.idCategoryStore;
      categorystoreModel.delete(idCategoryStore, function (error, results) {
        if (results && results.status) {
          res.json({
            status: true,
            "Message": "The categorystore has been deleted successfully"
          });
        } else {
          res.json({
            status: false,
            "Message": "The categorystore hasn't been deleted successfully"
          });
        }
      });
    })
    .put(function (req, res) {
      var idCategoryStore = req.params.idCategoryStore;
      var data = {
        idCategoryStore: req.body.idCategoryStore,
        idCateogry: req.body.idCateogry,
        idStore: req.body.idStore
      }
      if(idCategoryStore === data.idCategoryStore) {
        categorystoreModel.update(data, function(error, results) {
          if(results) {
            res.json({
              status: true,
              "Message": "The categorystore has been updated successfully"
            });
          } else {
            res.json({
              status: false,
              "Message": "The categorystore hasn't been updated successfully"
            });
          }
        });
      } else {
        res.json({
              status: false,
              "Message": "The categorystore's id doesn't match"
            });
      }
    });

    categorystoreRouter.route('/api/ws/mc/categorystore/bystore/:idStore/')
      .get(function(req, res) {
        var idStore = req.params.idStore;
        categorystoreModel.getOneByStore(idStore, function(results) {
          if (typeof results !== undefined) {
            res.json(results);
          } else {
            res.json({
              status: false,
              "Message" : "The categories store your are searching does not exist",
            });
          }
        });
      })

  module.exports = categorystoreRouter;
