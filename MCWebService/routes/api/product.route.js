var express = require('express');
var productModel = require('../../model/product.model');
var productRouter = express.Router();

productRouter.route('/api/ws/mc/product/')
  .get(function(req, res) {
      productModel.getAll(function(results) {
        if (typeof results !== undefined) {
          res.json(results);
        } else {
          res.json({
            status: false,
            "Message" : "There are no products",
          });
        }
      });
    })
  .post(function (req, res) {
    var data = {
        idCategory: req.body.idCategory,
        name: req.body.name,
        idCategory: req.body.idCategory,
        price: req.body.price,
        quantity: req.body.quantity,
        idStore: req.body.idStore,
        observations: req.body.observations,
        image: req.body.image
    }
    productModel.insert(data, function (error, results) {
      if(results && results.status) {
        res.json({
            status: true,
            "Message": "There product has been inserted successfully",
          });
      } else {
        res.json({
            status: true,
            "Message": "There product hasn't been inserted successfully",
          });
      }
    });
  });

  productRouter.route('/api/ws/mc/product/:idProduct/')
    .get(function(req, res) {
      var idProduct = req.params.idProduct;
      productModel.getOne(idProduct, function(results) {
        if (typeof results !== undefined) {
          res.json(results);
        } else {
          res.json({
            status: false,
            "Message" : "The product your are searching does not exist",
          });
        }
      });
    })
    .delete(function (req, res) {
      var idProduct = req.params.idProduct;
      productModel.delete(idProduct, function (error, results) {
        if (results && results.status) {
          res.json({
            status: true,
            "Message": "The product has been deleted successfully"
          });
        } else {
          res.json({
            status: false,
            "Message": "The product hasn't been deleted successfully"
          });
        }
      });
    })
    .put(function (req, res) {
      var data = {
        idProduct: req.params.idProduct,
        name: req.body.name,
        idCategory: req.body.idCategory,
        price: req.body.price,
        quantity: req.body.quantity,
        idStore: req.body.idStore,
        observations: req.body.observations,
        image: req.body.image
      }
        productModel.update(data, function(error, results) {
            res.json({
              status: true,
              "Message": "The product has been updated successfully"
            });
        });
       
    });

    productRouter.route('/api/ws/mc/product/bystore/:idStore/')
      .get(function(req, res) {
        var idStore = req.params.idStore;
        productModel.getOneByStore(idStore, function(results) {
          if (typeof results !== undefined) {
            res.json(results);
          } else {
            res.json({
              status: false,
              "Message" : "The product by store your are searching does not exist",
            });
          }
        });
      });

    productRouter.route('/api/ws/mc/product/bycategory/:idCategory/:idStore')
      .get(function(req, res) {
        console.log("El id de la categoria" + req.params.idCategory);
        console.log("El id de la tienda" + req.params.idStore);
          var data = {
            idCategory: req.params.idCategory,
            idStore: req.params.idStore
          }
          productModel.getOneByCategory(data, function(results) {
            if (typeof results !== undefined) {
              res.json(results);
            } else {
              res.json({
                status: false,
                "Message" : "The product by categeory your are searching does not exist",
              });
            }
          });
        });
  module.exports = productRouter;
