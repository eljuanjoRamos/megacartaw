var express = require('express');
var categoryModel = require('../../model/category.model');
var typeRoute = express.Router();

typeRoute.route('/api/ws/mc/category/')
  .get(function(req, res) {
      categoryModel.getAll(function(results) {
        if (typeof results !== undefined) {
          res.json(results);
        } else {
          res.json({
            status: false,
            "Message" : "There are no categories",
          });
        }
      });
    })
  .post(function (req, res) {
    var data = {
        idCategory: null,
        name: req.body.name
    }
    categoryModel.insert(data, function (error, results) {
      if(results && results.status) {
        res.json({
            status: true,
            "Message": "There category has been inserted successfully",
          });
      } else {
        res.json({
            status: true,
            "Message": "There category hasn't been inserted successfully",
          });
      }
    });
  });

  

  typeRoute.route('/api/ws/mc/category/:idCategory/')
    .get(function(req, res) {
      var idCategory = req.params.idCategory;
      categoryModel.getOne(idCategory, function(results) {
        if (typeof results !== undefined) {
          res.json(results);
        } else {
          res.json({
            status: false,
            "Message" : "The category your are searching does not exist",
          });
        }
      });
    })
    .delete(function (req, res) {
      var idCategory = req.params.idCategory;
      categoryModel.delete(idCategory, function (error, results) {
        if (results && results.status) {
          res.json({
            status: true,
            "Message": "The category has been deleted successfully"
          });
        } else {
          res.json({
            status: false,
            "Message": "The category hasn't been deleted successfully"
          });
        }
      });
    })
    .put(function (req, res) {
      var idCategory = req.params.idCategory;
      var data = {
        idCategory: req.body.idCategory,
        name: req.body.name
      }
      if(idCategory === data.idCategory) {
        categoryModel.update(data, function(error, results) {
          if(results && results.status) {
            res.json({
              status: true,
              "Message": "The category has been updated successfully"
            });
          } else {
            res.json({
              status: false,
              "Message": "The category hasn't been updated successfully"
            });
          }
        });
      } else {
        res.json({
              status: false,
              "Message": "The category's id doesn't match"
            });
      }
    });

  module.exports = typeRoute;
