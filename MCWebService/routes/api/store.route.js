var express = require('express');
var storeModel = require('../../model/store.model');
var storeRoute = express.Router();

storeRoute.route('/api/ws/mc/idstore/:idStore')
  .get(function(req, res) {
      var idUsuario = req.params.idStore;
      storeModel.getIdStore(idUsuario, function(results) {
        if (typeof results !== undefined) {
          res.json(results);
        } else {
          res.json({
            status: false,
            "Message" : "There are no stores",
          });
        }
      });
});



storeRoute.route('/api/ws/mc/store/')
  .get(function(req, res) {
      storeModel.getAll(function(results) {
        if (typeof results !== undefined) {
          res.json(results);
        } else {
          res.json({
            status: false,
            "Message" : "There are no stores",
          });
        }
      });
    })
  .post(function (req, res) {
    var data = {
        idStore: null,
        name: req.body.name,
        email: req.body.email,
        address: req.body.address,
        phone: req.body.phone,
        image: req.body.image
    }
    storeModel.insert(data, function (error, results) {
      if(results && results.status) {
        res.json({
            status: true,
            "Message": "There store has been inserted successfully",
          });
      } else {
        res.json({
            status: true,
            "Message": "There store hasn't been inserted successfully",
          });
      }
    });
  });

  storeRoute.route('/api/ws/mc/store/:idStore/')
    .get(function(req, res) {
      var idStore = req.params.idStore;
      storeModel.getOne(idStore, function(results) {
        if (typeof results !== undefined) {
          res.json(results);
        } else {
          res.json({
            status: false,
            "Message" : "The store your are searching does not exist",
          });
        }
      });
    })
    .delete(function (req, res) {
      var idStore = req.params.idStore;
      storeModel.delete(idStore, function (error, results) {
        if (results && results.status) {
          res.json({
            status: true,
            "Message": "The store has been deleted successfully"
          });
        } else {
          res.json({
            status: false,
            "Message": "The store hasn't been deleted successfully"
          });
        }
      });
    })
    .put(function (req, res) {
      var idStore = req.params.idStore;
      var data = {
        idStore: req.body.idStore,
        name: req.body.name,
        email: req.body.email,
        address: req.body.address,
        phone: req.body.phone,
        image: req.body.image
      }
      if(idStore === data.idStore) {
        storeModel.update(data, function(error, results) {
          if(results && results.status) {
            res.json({
              status: true,
              "Message": "The store has been updated successfully"
            });
          } else {
            res.json({
              status: false,
              "Message": "The store hasn't been updated successfully"
            });
          }
        });
      } else {
        res.json({
              status: false,
              "Message": "The store's id doesn't match"
            });
      }
    });

  module.exports = storeRoute;
