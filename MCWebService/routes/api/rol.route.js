var express = require('express');
var router = express.Router();
var services = require('../../services');
var rol = require('../../model/rol.model');

router.get('/rol/', function(req, res, next) {
  rol.selectAll(function(error, resultados) {
    if(typeof resultados !== 'undefined') {
      res.json(resultados);
    } else {
      res.json({"mensaje" : "No hay roles"});
    }
  });
});

module.exports = router;
