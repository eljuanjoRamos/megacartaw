var express = require('express');
var carrito = require('../../model/carrito.model');
var services = require('../../services');
var router = express.Router();

router.get('/carrito/', services.verificar, function(req, res, next) {
  var idUsuario = req.usuario.idUsuario;
      carrito.selectAll(idUsuario, function(error, resultados){
        if(typeof resultados !== undefined) {
          res.json(resultados);
        } else {
          res.json({"Mensaje": "No hay carrito"});
        }
      });
});

router.post('/carrito', services.verificar, function(req, res, next) {
  var data = {
    idUsuario: req.usuario.idUsuario,
    idProduct: req.body.idProduct,
    cantidad: req.body.cantidad
  };

  carrito.insert(data, function(resultado){
    if(resultado && resultado.affectedRows > 0) {
      res.json({
        estado: true,
        mensaje: "Se agrego el carrito"
      });
    } else {
      res.json({"mensaje":"No se agrego el carrito"});
    }
  });
});

router.put('/carrito/:idCarrito', services.verificar, function(req, res, next){
  var idCarrito = req.params.idCarrito;
  var data = {
    idCarrito : idCarrito,
    idEstado : req.body.idEstado
  }
  carrito.update(data, function(resultado){
    if(resultado.length > 0) {
      res.json({
        estado: true,
        mensaje: "Se ha modificado con exito"
      });
    } else {
      res.json({
        estado: false,
        mensaje: "No se pudo modificar"
      });
    }
  });
});

router.delete('/carrito/:idCarrito', services.verificar, function(req, res, next){
  var idCarrito = req.params.idCarrito;
  var data = {
      idCarrito: idCarrito
  }
  carrito.delete(data, function(resultado){
    if(resultado && resultado.mensaje ===	"Eliminado") {
      res.json({
        estado: true,
        "mensaje":"Se elimino el carrito correctamente"
      });
    } else {
      res.json({
        estado: false,
        "mensaje":"No se elimino el carrito"});
    }
  });
});

module.exports = router;
