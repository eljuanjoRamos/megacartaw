var express = require('express');
var router = express.Router();
var services = require('../../services');
var estado = require('../../model/estado.model');

router.get('/estado/', function(req, res, next) {
  estado.selectAll(function(error, resultados) {
    if(typeof resultados !== 'undefined') {
      res.json(resultados);
    } else {
      res.json({"mensaje" : "No hay estados"});
    }
  });
});

module.exports = router;
