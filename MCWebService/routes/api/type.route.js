var express = require('express');
var typeModel = require('../../model/type.model');
var typeRoute = express.Router();

typeRoute.route('/api/ws/mc/type/')
  .get(function(req, res) {
      typeModel.getAll(function(results) {
        if (typeof results !== undefined) {
          res.json(results);
        } else {
          res.json({
            status: false,
            "Message" : "There are no types",
          });
        }
      });
    })
  .post(function (req, res) {
    var data = {
        idType: null,
        name: req.body.name,
        image: req.body.image
    }
    typeModel.insert(data, function (error, results) {
      if(results && results.status) {
        res.json({
            status: true,
            "Message": "There type has been inserted successfully",
          });
      } else {
        res.json({
            status: true,
            "Message": "There type hasn't been inserted successfully",
          });
      }
    });
  });

  typeRoute.route('/api/ws/mc/type/:idType/')
    .get(function(req, res) {
      var idType = req.params.idType;
      typeModel.getOne(idType, function(results) {
        if (typeof results !== undefined) {
          res.json(results);
        } else {
          res.json({
            status: false,
            "Message" : "The type your are searching does not exist",
          });
        }
      });
    })
    .delete(function (req, res) {
      var idType = req.params.idType;
      typeModel.delete(idType, function (error, results) {
        if (results && results.status) {
          res.json({
            status: true,
            "Message": "The type has been deleted successfully"
          });
        } else {
          res.json({
            status: false,
            "Message": "The type hasn't been deleted successfully"
          });
        }
      });
    })
    .put(function (req, res) {
      var idType = req.params.idType;
      var data = {
        idType: req.body.idType,
        name: req.body.name,
        image: req.body.image
      }
      if(idType === data.idType) {
        typeModel.update(data, function(error, results) {
          if(results && results.status) {
            res.json({
              status: true,
              "Message": "The type has been updated successfully"
            });
          } else {
            res.json({
              status: false,
              "Message": "The type hasn't been updated successfully"
            });
          }
        });
      } else {
        res.json({
              status: false,
              "Message": "The type's id doesn't match"
            });
      }
    });

  module.exports = typeRoute;
