var express = require('express');
var bodyParser = require('body-parser');
var logger = require('morgan');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

//IMPORTAR ROUTES
var typeRoute = require('./routes/api/type.route');
var storetypeRoute = require('./routes/api/storetype.route');
var storeRoute = require('./routes/api/store.route');
var categoryRoute = require('./routes/api/category.route');
var productRoute = require('./routes/api/product.route');
var categorystoreRoute = require('./routes/api/categorystore.route');
var interfaceRoute = require('./routes/interface.route');
var carritoRoute = require('./routes/api/carrito.route');
var estadoRoute = require('./routes/api/estado.route');
var rolRoute = require('./routes/api/rol.route');
var usuarioRoute = require('./routes/api/usuario.route');
var authRoute = require('./routes/authenticate.route');
var app = express();
var port = 3000;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//CONFIGURACION DE BODY-PARSER
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/jquery', express.static(path.join(__dirname, 'node_modules/jquery/dist')));

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
    next();
});

app.use('/', typeRoute);
app.use('/', storetypeRoute);
app.use('/', storeRoute);
app.use('/', categoryRoute);
app.use('/', productRoute);
app.use('/', categorystoreRoute);
app.use('/', interfaceRoute);
app.use('/', carritoRoute);
app.use('/', estadoRoute);
app.use('/', rolRoute);
app.use('/', usuarioRoute);
app.use('/', authRoute);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(port, function() {
  console.log("The server is running at: " + port);
});