var mysql = require('mysql');

var params =  {
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'MC'
}

var connection = mysql.createConnection(params);

module.exports = connection;
