var database = require("../config/database.config");
var Carrito = {};

Carrito.selectAll = function(idUsuario, callback) {
  if(database) {
		database.query('CALL SP_SelectCarrito(?)', function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(null, resultados);
			}
		});
	}
}

Carrito.insert = function(data, callback) {
  if(database) {
    database.query('CALL SP_InsertarCarrito(?, ?, ?)',
    [data.idUsuario, data.idProduct, data.cantidad],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

Carrito.update = function(data, callback){
	if(database) {
		database.query('CALL SP_EditarCarrito(?, ?)',
		[data.idCarrito, data.idEstado],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado[0]);
			}
		});
	}
}

Carrito.delete = function(data, callback) {
	if(database) {
		database.query('CALL SP_EliminarCarrito(?)', [data.idCarrito],
		function(error, resultado){
			if(error){
				throw error;
			} else {
				callback({"mensaje":"Eliminado"});
			}
		});
	}
}

module.exports = Carrito;
