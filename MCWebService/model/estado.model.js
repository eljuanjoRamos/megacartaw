var database = require("../config/database.config");
var Estado = {};

Estado.selectAll = function(callback) {
  if(database) {
		database.query('SELECT * FROM Estado', function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(null, resultados);
			}
		});
	}
}

module.exports = Estado;
