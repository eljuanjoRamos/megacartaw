var database = require("../config/database.config");
var Usuario = {};

Usuario.login = function(data, callback) {
  if(database) {
    var consulta = 'CALL SP_Autenticar(?, ?)';
		database.query(consulta, [data.nick, data.pass], function(error, resultado){
			if(error) {
				throw error;
			} else {
				if(resultado[0].length > 0) {
					callback(resultado[0]);
				} else {
					callback(0);
				}
			}
		});
	}
}

Usuario.selectAll = function(callback) {
	if(database) {
		var consulta = 'SELECT * FROM Usuario';
		database.query(consulta, function(error, resultado){
			if(error) throw error;
			callback(resultado);
		});
	}
}

Usuario.selectUsuario = function(idUsuario, callback) {
  if(database) {
    var consulta = 'SELECT * FROM Usuario WHERE idUsuario = ?';
		database.query(consulta, idUsuario, function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado[0]);
			}
		});
	}
}

Usuario.selectHistorial = function(idUsuario, callback) {
  if(database) {
		var query = "SELECT * FROM VIEW_HistorialUsuario WHERE idUsuario = ?";
    database.query(query, idUsuario,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });
  }
}

Usuario.selectMiUsuario = function(idUsuario, callback) {
  if(database) {
		var query = "SELECT * FROM VIEW_Usuario WHERE idUsuario = ?";
    database.query(query, idUsuario,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });
  }
}

Usuario.insert = function(data, callback) {
  if(database) {
    database.query('CALL SP_InsertarUsuario(?, ?, ?, ?, ?, ?, ?)',
    [data.nick, data.contrasena, data.nombre, data.apellido, data.direccion,
    data.telefono, data.nit],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback({"affectedRows": resultado.affectedRows});
      }
    });
  }
}

Usuario.update = function(data, callback){
	if(database) {
		database.query('CALL SP_EditarUsuario(?, ?, ?, ?, ?, ?, ?, ?)',
		[data.idUsuario, data.nick, data.contrasena, data.nombre, data.apellido,
      data.direccion, data.telefono, data.nit],
		function(error, resultado){
			if(error) {
				throw error;
			} else {
				callback(resultado[0]);
			}
		});
	}
}

module.exports = Usuario;
