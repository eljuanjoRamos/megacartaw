var db = require('../config/database.config');
var storetype = {}

storetype.getAll = function (callback) {
  if (db) {
    db.query("SELECT * FROM ShowStoreType;", function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

storetype.getOne = function (idType, callback) {
  if (db) {
    db.query("SELECT * FROM ShowStoreTypeId WHERE idType = ?;", idType, function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

storetype.delete = function (idType, callback) {
  if (db) {
    db.query("CALL DeleteStoreType(?);", idType, function (error, results) {
      if(error) throw error;
      callback(null, {status:true});
    });
  }
}


storetype.insert = function(data, callback) {
  if(db) {
    var query = "CALL InsertStoreType(" + [data.idStore] + ", " + [data.idType] + ");";
    db.query(query, data, function (error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}

storetype.update = function(data, callback) {
  if(db) {
    db.query("CALL UpdateStoreType(?, ?, ?);", [data.idStore, 
                                data.idType, data.idStoreType], function(error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}

module.exports = storetype;
