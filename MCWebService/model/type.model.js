var db = require('../config/database.config');
var type = {}

type.getAll = function (callback) {
  if (db) {
    db.query("SELECT Type.idType, Type.name, Type.image, COUNT(*) AS Cantidad FROM StoreType INNER JOIN Type ON StoreType.idType = Type.idType GROUP BY idType", function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

type.getOne = function (idType, callback) {
  if (db) {
    db.query("SELECT * FROM ShowOneType WHERE idType = ?;", idType, function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

type.delete = function (idType, callback) {
  if (db) {
    db.query("CALL DeleteType(?);", idType, function (error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}

type.insert = function(data, callback) {
  if(db) {
    db.query("CALL InsertType(?, ?);", [data.name, data.image], function (error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}

type.update = function(data, callback) {
  if(db) {
    db.query("CALL updatetype(?, ?, ?);", [data.name, data.image, data.idType], function(error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}

module.exports = type;
