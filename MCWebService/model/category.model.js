var db = require('../config/database.config');
var category = {}

category.getAll = function (callback) {
  if (db) {
    db.query("SELECT * FROM Category", function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

category.insert = function(data, callback) {
  if(db) {
    db.query("CALL InsertCategory(?);", [data.name], function (error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}

category.getOne = function (idCategory, callback) {
  if (db) {
    db.query("SELECT * FROM ShowOneCategory WHERE idCategory = ?;", idCategory, function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

category.delete = function (idCategory, callback) {
  if (db) {
    db.query("CALL DeleteCategory(?);", idCategory, function (error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}

category.update = function(data, callback) {
  if(db) {
    db.query("CALL UpdateCategory(?, ?);", [data.name, 
                                  data.idCategory], function(error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}

module.exports = category;
