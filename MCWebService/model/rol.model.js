var database = require("../config/database.config");
var Rol = {};

Rol.selectAll = function(callback) {
  if(database) {
		database.query('SELECT * FROM Rol', function(error, resultados){
			if(error) {
				throw error;
			} else {
				callback(null, resultados);
			}
		});
	}
}

module.exports = Rol;
