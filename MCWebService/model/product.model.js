var db = require('../config/database.config');
var product = {}

product.getAll = function (callback) {
  if (db) {
    db.query("SELECT * FROM ShowProducts;", function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

product.insert = function(data, callback) {
  if(db) {
    db.query("CALL InsertProduct(?, ?, ?, ?, ?, ?, ?);", [data.name, data.idCategory,
                                    data.price, data.quantity, 
                                    data.idStore, data.observations, data.image], function (error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}


product.getOneByCategory = function (data, callback) {
  if (db) {

    console.log(data);
    var query = "SELECT p.idProduct, p.name AS nombreProducto, p.idCategory, c.name AS categroyname, p.price, p.quantity, p.idStore, p.observations, p.image  FROM Product p INNER JOIN Category c ON c.idCategory = p.idCategory INNER JOIN Store s ON s.idStore = p.idStore WHERE p.idCategory =? AND p.idStore =?"
    db.query(query,[data.idCategory, data.idStore], function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

product.getOne = function (idProduct, callback) {
  if (db) {
    db.query("SELECT * FROM Product WHERE idProduct = ?;", idProduct, function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

product.getOneByStore = function (idStore, callback) {
  if (db) {
    db.query("SELECT * FROM ShowProductsByStore WHERE idStore = ?;", idStore, function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

product.delete = function (idProduct, callback) {
  if (db) {
    db.query("CALL DeleteProduct(?);", idProduct, function (error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}

product.update = function(data, callback) {
  if(db) {
    db.query("CALL UpdateProduct(?, ?, ?, ?, ?, ?, ?, ?);", [data.name, data.idCategory,
                                    data.price, data.quantity, data.idStore, 
                                    data.observations, data.image,
                                    data.idProduct], function(error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}

module.exports = product;
