var db = require('../config/database.config');
var categorystore = {}

categorystore.getAll = function (callback) {
  if (db) {
    db.query("SELECT * FROM ShowCategoryStores;", function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

categorystore.getOne = function (idCategoryStore, callback) {
  if (db) {
    db.query("SELECT * FROM ShowOneCategoryStore WHERE idCategoryStore = ?;", idCategoryStore, function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

categorystore.getOneByStore = function (idStore, callback) {
  if (db) {
    db.query("SELECT * FROM ShowCategoryStoreId WHERE idStore = ?;", idStore, function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

categorystore.delete = function (idCategoryStore, callback) {
  if (db) {
    db.query("CALL DeleteCategoryStore(?);", idCategoryStore, function (error, results) {
      if(error) throw error;
      callback(null, {status:true});
    });
  }
}


categorystore.insert = function(data, callback) {
  if(db) {
    var query = "CALL InsertCategoryStore(" + [data.idCategory] + ", " + [data.idStore] + ");";
    db.query(query, data, function (error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}

categorystore.update = function(data, callback) {
  if(db) {
    db.query("CALL UpdateCategoryStore(?, ?, ?);", [data.idCategory, 
                                data.idStore, data.idCategoryStore], function(error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}

module.exports = categorystore;
