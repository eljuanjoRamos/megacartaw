var db = require('../config/database.config');
var store = {}

store.getAll = function (callback) {
  if (db) {
    db.query("SELECT * FROM Store", function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

store.getIdStore = function (idUsuario, callback) {
  if (db) {
    db.query("select idStore from administradortienda where idUsuario = ?", idUsuario,function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

store.insert = function(data, callback) {
  if(db) {
    db.query("CALL InsertStore(?, ?, ?, ?, ?);", [data.name, data.email,
                                        data.address, data.phone,
                                        data.image], function (error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}

store.getOne = function (idStore, callback) {
  if (db) {
    db.query("SELECT * FROM ShowOneStore WHERE idStore = ?;", idStore, function (error, results) {
      if (error) throw error;
      callback(results);
    });
  }
}

store.delete = function (idStore, callback) {
  if (db) {
    db.query("CALL DeleteStore(?);", idStore, function (error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}

store.update = function(data, callback) {
  if(db) {
    db.query("CALL UpdateStore(?, ?, ?, ?, ?, ?);", [data.name, data.email,
                              data.address, data.phone,
                              data.image, data.idStore], function(error, results) {
      if(error) throw error;
      callback(null, {status: true});
    });
  }
}

module.exports = store;
