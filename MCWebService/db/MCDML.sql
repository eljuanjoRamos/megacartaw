INSERT INTO `mc`.`type` (`name`, `image`) VALUES ('Zapatos', 'https://www.profedeele.es/wp-content/uploads/2016/11/ropa-tendida.jpg');
INSERT INTO `mc`.`type` (`name`, `image`) VALUES ('Ropa', 'https://www.profedeele.es/wp-content/uploads/2016/11/ropa-tendida.jpg');
INSERT INTO `mc`.`type` (`name`, `image`) VALUES ('Comida', 'http://cdn3.upsocl.com/wp-content/uploads/2015/04/comida-mexicana.jpg');
INSERT INTO `mc`.`type` (`name`, `image`) VALUES ('Adornos', 'http://3.bp.blogspot.com/-M4Q4HRkPFh4/UM8ZZ_lzrII/AAAAAAAA2M8/VcFb47G8REU/s1600/ADORNOS+%2528757%2529.png');
INSERT INTO `mc`.`type` (`name`, `image`) VALUES ('Utiles escolares', 'http://www.colegiorayenmahuida.cl/wp-content/uploads/utiles-escolares.jpg');
INSERT INTO `mc`.`type` (`name`, `image`) VALUES ('Carros', 'http://4.bp.blogspot.com/-YNdGnWiVQuk/V9KTO4i02hI/AAAAAAAAHic/d_7wTz5skToc03f1YdiMGS3cRp0gCweSgCK4B/s1600/Solo-Carros.jpg');
INSERT INTO `mc`.`type` (`name`, `image`) VALUES ('Motos', 'https://www.bmw-drivingexperience.com/content/dam/bmw-group-websites/drivex_com/4_Fahrzeuge/motorrad_kb.png.grp-transform/large/motorrad_kb.png');
INSERT INTO `mc`.`type` (`name`, `image`) VALUES ('Tecnologia', 'http://www.cosasdearquitectos.com/wp-content/uploads/internet-de-las-cosas-arquitectura-1024x655.jpg');
INSERT INTO `mc`.`type` (`name`, `image`) VALUES ('Belleza', 'http://www.italianfashionagency.com/images/Beauty2.jpg');
INSERT INTO `mc`.`type` (`name`, `image`) VALUES ('Deportes', 'http://www.imdsg.es/wp-content/uploads/fondo.jpg');

INSERT INTO `mc`.`store` (`name`, `email`, `address`, `phone`, `image`) VALUES ('Nike', 'nike@email.com', 'Zona 14', '12345678', 'http://news.nike.com/images/swoosh-default.jpg');
INSERT INTO `mc`.`store` (`name`, `email`, `address`, `phone`, `image`) VALUES ('Adidas', 'adidas@email.com', 'Zona 20', '12345678', 'https://suppastore-rancher.s3.amazonaws.com/brands/thumbnail/36/adidas-black_1.png');
INSERT INTO `mc`.`store` (`name`, `email`, `address`, `phone`, `image`) VALUES ('Zara', 'zara@email.com', 'Zona 34', '12345678', 'http://1000logos.net/wp-content/uploads/2017/05/Zara-Symbol.jpg');
INSERT INTO `mc`.`store` (`name`, `email`, `address`, `phone`, `image`) VALUES ('Distefano', 'distefano@email.com', 'Zona 10', '12345678', 'https://media.merchantcircle.com/25796927/Logo_full.jpeg');
INSERT INTO `mc`.`store` (`name`, `email`, `address`, `phone`, `image`) VALUES ('Intelaf', 'intelaf', 'Zona 9', '12345678', 'http://www.ciudadsantaclara.com/sites/default/files/santa-clara-tiendas-intelaf.jpg');
INSERT INTO `mc`.`store` (`name`, `email`, `address`, `phone`, `image`) VALUES ('Walmart', 'walmart@email.com', 'Zona 13', '12345678', 'https://cdn.corporate.walmart.com/resource/assets-bsp3/images/corp/walmart-logo.64968e7648c4bbc87f823a1eff1d6bc7.png');
INSERT INTO `mc`.`store` (`name`, `email`, `address`, `phone`, `image`) VALUES ('Grupo los tres', 'glt@email.com', 'Zona 9', '12345678', 'http://grupolostres.net/seminuevos/gt/images/semi.png');
INSERT INTO `mc`.`store` (`name`, `email`, `address`, `phone`, `image`) VALUES ('Mc Donalds', 'mcdonald@email.com', 'Zona 21', '12345678', 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Mcdonalds-90s-logo.svg/2000px-Mcdonalds-90s-logo.svg.png');
INSERT INTO `mc`.`store` (`name`, `email`, `address`, `phone`, `image`) VALUES ('Demuseo', 'demuseo@email.com', 'Zona 31', '12345678', 'https://demuseo.com/wp-content/uploads/2014/10/LogoDemuseoN.png');
INSERT INTO `mc`.`store` (`name`, `email`, `address`, `phone`, `image`) VALUES ('Jungle', 'jungle@email.com', 'Zona 10', '12345678', 'https://www.facebook.com/JUNGLE.SKATESHOPS/photos/a.10150661679994455.414300.87341714454/10150661680044455/?type=3');

INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('1', '1');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('1', '2');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('1', '10');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('2', '1');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('2', '2');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('2', '10');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('3', '1');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('3', '2');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('3', '9');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('4', '2');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('4', '9');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('5', '8');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('6', '1');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('6', '2');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('6', '3');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('6', '4');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('6', '5');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('6', '8');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('6', '9');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('6', '10');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('7', '6');
INSERT INTO `mc`.`storetype` (`idStore`, `idType`) VALUES ('7', '7');

INSERT INTO `mc`.`category` (`name`) VALUES ('Hombre');
INSERT INTO `mc`.`category` (`name`) VALUES ('Mujer');
INSERT INTO `mc`.`category` (`name`) VALUES ('Niño');
INSERT INTO `mc`.`category` (`name`) VALUES ('Bebe');
INSERT INTO `mc`.`category` (`name`) VALUES ('Skate');
INSERT INTO `mc`.`category` (`name`) VALUES ('Futbol');
INSERT INTO `mc`.`category` (`name`) VALUES ('Natacion');
INSERT INTO `mc`.`category` (`name`) VALUES ('Pintauñas');
INSERT INTO `mc`.`category` (`name`) VALUES ('Depiladores');
INSERT INTO `mc`.`category` (`name`) VALUES ('Maquillajes');
INSERT INTO `mc`.`category` (`name`) VALUES ('Mouses');

INSERT INTO `mc`.`categorystore` (`idStore`, `idCategory`) VALUES ('1', '1');
INSERT INTO `mc`.`categorystore` (`idStore`, `idCategory`) VALUES ('1', '2');
INSERT INTO `mc`.`categorystore` (`idStore`, `idCategory`) VALUES ('1', '3');
INSERT INTO `mc`.`categorystore` (`idStore`, `idCategory`) VALUES ('1', '4');
INSERT INTO `mc`.`categorystore` (`idStore`, `idCategory`) VALUES ('1', '5');
INSERT INTO `mc`.`categorystore` (`idStore`, `idCategory`) VALUES ('2', '1');
INSERT INTO `mc`.`categorystore` (`idStore`, `idCategory`) VALUES ('2', '2');
INSERT INTO `mc`.`categorystore` (`idStore`, `idCategory`) VALUES ('2', '5');
INSERT INTO `mc`.`categorystore` (`idStore`, `idCategory`) VALUES ('2', '6');
INSERT INTO `mc`.`categorystore` (`idStore`, `idCategory`) VALUES ('3', '1');
INSERT INTO `mc`.`categorystore` (`idStore`, `idCategory`) VALUES ('3', '2');
INSERT INTO `mc`.`categorystore` (`idStore`, `idCategory`) VALUES ('3', '3');
INSERT INTO `mc`.`categorystore` (`idStore`, `idCategory`) VALUES ('3', '8');
INSERT INTO `mc`.`categorystore` (`idStore`, `idCategory`) VALUES ('3', '9');
INSERT INTO `mc`.`categorystore` (`idStore`, `idCategory`) VALUES ('3', '10');

INSERT INTO `mc`.`product` (`name`, `idCategory`, `idStore`, `observations`) VALUES ('Mouse', '11', '5', 'Color: rojo. Marca: Microsoft.');
INSERT INTO `mc`.`product` (`name`, `idCategory`, `idStore`, `observations`) VALUES ('Nike Zoom Air', '1', '1', 'Size: US 10');
INSERT INTO `mc`.`product` (`name`, `idCategory`, `idStore`, `observations`) VALUES ('Nike Stefan Janoski', '3', '1', 'Size: EU 20');
INSERT INTO `mc`.`product` (`name`, `idCategory`, `idStore`, `observations`) VALUES ('Adidas Super Star', '2', '2', 'Size: 50, Color: Blue');
INSERT INTO `mc`.`product` (`name`, `idCategory`, `idStore`, `observations`) VALUES ('Pantalon Formal', '4', '3', 'Size: 30');
