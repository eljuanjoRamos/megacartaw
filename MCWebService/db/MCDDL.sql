DROP DATABASE IF EXISTS MC;
CREATE DATABASE MC;
USE MC;

CREATE TABLE Type(
	idType INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR (50) NOT NULL,
    image TEXT
);

CREATE TABLE Store (
	idStore INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR (50) NOT NULL,
    email VARCHAR (50) NOT NULL,
    address VARCHAR (50) NOT NULL,
    phone VARCHAR (50) NOT NULL, 
    image TEXT
);

CREATE TABLE StoreType(
	idStoreType INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    idStore INT NOT NULL,
    idType INT NOT NULL,
    FOREIGN KEY(idStore) REFERENCES Store (idStore) ON DELETE CASCADE,
    FOREIGN KEY(idType) REFERENCES Type (idType) ON DELETE CASCADE
);

CREATE TABLE Usuario(
	idUsuario INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nick VARCHAR(255) NOT NULL,
    pass VARCHAR(255) NOT NULL
);



CREATE TABLE Category (
	idCategory INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR (50) NOT NULL
);

CREATE TABLE CategoryStore (
	idCategoryStore INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    idCategory INT NOT NULL,
    idStore INT NOT NULL,
    FOREIGN KEY(idCategory) REFERENCES Category (idCategory) ON DELETE CASCADE,
    FOREIGN KEY(idStore) REFERENCES Store(idStore) ON DELETE CASCADE
);

CREATE TABLE Product (
	idProduct INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    idCategory INT NOT NULL,
    idStore INT NOT NULL,
	observations TEXT,
    FOREIGN KEY (idCategory) REFERENCES Category (idCategory) ON DELETE CASCADE,
    FOREIGN KEY (idStore) REFERENCES Store (idStore) ON DELETE CASCADE
);

 #
 #
 #BEGINING STORED PROCEDURES
 #
 #
 
 DELIMITER $$
 CREATE PROCEDURE ShowStoreTypeId(IN idtyp INT)
	BEGIN
		SELECT st.idStoreType, st.idStore, s.name, s.email, s.address, 
		s.phone, s.image ,st.idType, t.name FROM 
		StoreType st 
		INNER JOIN Store s ON s.idStore = st.idStore
		INNER JOIN Type t ON t.idType = st.idType WHERE st.idType = idtyp;
    END $$
 DELIMITER ;

DELIMITER $$
 CREATE PROCEDURE ShowStoreId(IN idcatstor INT)
	BEGIN
		SELECT cs.idCategoryStore, cs.idStore, s.name, s.email, s.address, 
		s.phone, s.image, cs.idCategory, c.name FROM 
		CategoryStore cs
		INNER JOIN Store s ON s.idStore = cs.idStore
		INNER JOIN Category c ON c.idCategory = cs.idCategory WHERE cs.idStore = idcatstor;
    END $$
 DELIMITER ;

DELIMITER $$
 CREATE PROCEDURE ShowProducts(IN idcat INT, IN idsto INT)
	BEGIN
		SELECT p.idProduct, p.name, p.idCategory, c.name, p.idStore, s.name, p.observations FROM
		Product p
		INNER JOIN Category c ON c.idCategory = p.idCategory
		INNER JOIN Store s ON s.idStore = p.idStore WHERE P.idCategory = idcat AND p.idStore = idsto;
    END $$
 DELIMITER ;
 
 #
 #
 #STORE STORED PROCEDURES
 #
 #
 
  DELIMITER $$
 CREATE PROCEDURE InsertStore(IN n VARCHAR(50), IN e VARCHAR(50), IN a VARCHAR(50), IN p VARCHAR(50),
								IN i VARCHAR(50))
	BEGIN
		 INSERT INTO Store (name, email, address, phone, image) VALUES (n, e, a, p, i);
    END $$
 DELIMITER ;
 
   DELIMITER $$
 CREATE PROCEDURE ShowOneStore(IN id INT)
	BEGIN
		SELECT * FROM Store WHERE idStore = id; 
    END $$
 DELIMITER ;
 
 DELIMITER $$
 CREATE PROCEDURE DeleteStore(IN id INT)
	BEGIN
		 DELETE FROM Store WHERE idStore = id;
    END $$
 DELIMITER ;
 
  DELIMITER $$
 CREATE PROCEDURE UpdateStore(IN n VARCHAR(50), IN e VARCHAR(50), IN a VARCHAR(50), IN p VARCHAR(50),
								IN i VARCHAR(50), IN id INT)
	BEGIN
		 UPDATE Store SET name = n, email = e, 
			address = a, phone = p, image = i WHERE idStore = id;
    END $$
 DELIMITER ;
 
 #
 #
 #STORETYPE STORED PROCEDURES
 #
 #
 
  DELIMITER $$
 CREATE PROCEDURE ShowStoreType()
	BEGIN
		SELECT st.idStoreType, st.idStore, s.name, s.email, s.address, 
		s.phone, s.image ,st.idType, t.name FROM 
		StoreType st 
		INNER JOIN Store s ON s.idStore = st.idStore
		INNER JOIN Type t ON t.idType = st.idType;
    END $$
 DELIMITER ;
 
 DELIMITER $$
 CREATE PROCEDURE InsertStoreType(IN idSto INT, IN idTyp INT)
	BEGIN
		 INSERT INTO StoreType (idStore, idType) VALUES (idSto, idTyp);
    END $$
 DELIMITER ;
 
 DELIMITER $$
 CREATE PROCEDURE UpdateStoreType(IN idSto INT, IN idTyp INT, IN id INT)
	BEGIN
		 UPDATE StoreType SET idStore = idSto, idType = idTyp WHERE idStoreType = id;
    END $$
 DELIMITER ;
 
   DELIMITER $$
 CREATE PROCEDURE DeleteStoreType(IN id INT)
	BEGIN
		 DELETE FROM StoreType WHERE idStoreType = id;
    END $$
 DELIMITER ;
 
 #
 #
 #TYPE STORED PROCEDURES
 #
 #
 
 DELIMITER $$
 CREATE PROCEDURE ShowOneType(IN id INT)
	BEGIN
		 SELECT * FROM Type WHERE idType = id;
    END $$
 DELIMITER ;
 
DELIMITER $$
 CREATE PROCEDURE DeleteType(IN id INT)
	BEGIN
    
        DECLARE idt INT;
        DECLARE gidt INT;
        
        SET idt = (SELECT EXISTS(SELECT * FROM Type WHERE type.name = 'Not assigned'));
		IF (idt != 0) THEN
				SELECT 'Es uno';
                SET gidt = (SELECT idType FROM Type WHERE Type.name = 'Not assigned');
                SELECT gidt;
                UPDATE StoreType SET idType = gidt WHERE StoreType.idType = id;
                DELETE FROM Type WHERE Type.idType = id;
			ELSE
				SELECT 'Es cero';
                INSERT INTO Type (name) VALUES ('Not assigned');
                SET gidt = (SELECT idType FROM Type WHERE Type.name = 'Not assigned');
                UPDATE StoreType SET idType = gidt WHERE StoreType.idType = id;
                DELETE FROM Type WHERE Type.idType = id;
            END IF;
    END $$
 DELIMITER ;
 
   DELIMITER $$
 CREATE PROCEDURE InsertType(IN n VARCHAR(50), IN i TEXT)
	BEGIN
		 INSERT INTO Type (name, image) VALUES (n, i);
    END $$
 DELIMITER ;
  
  DELIMITER $$
 CREATE PROCEDURE UpdateType(IN n VARCHAR(50), IN i TEXT, IN id INT)
	BEGIN
		 UPDATE Type SET name = n, image = i WHERE idType = id;
    END $$
 DELIMITER ;
 
 #
 #
 #CATEGORY STORED PROCEDURES
 #
 #
 
  DELIMITER $$
 CREATE PROCEDURE ShowOneCategory(IN id INT)
	BEGIN
		SELECT * FROM Category WHERE Category.idCategory = id;
    END $$
 DELIMITER ;
 
 DELIMITER $$
 CREATE PROCEDURE InsertCategory(IN n VARCHAR(50))
	BEGIN
		 INSERT INTO Category (name) VALUES (n);
    END $$
 DELIMITER ;
 
 DELIMITER $$
 CREATE PROCEDURE UpdateCategory(IN n VARCHAR(50), IN id INT)
	BEGIN
		 UPDATE Category SET name = n WHERE Category.idCategory = id;
    END $$
 DELIMITER ;
 
   DELIMITER $$
 CREATE PROCEDURE DeleteCategory(IN id INT)
	BEGIN
		 DELETE FROM Category WHERE Category.idCategory = id;
    END $$
 DELIMITER ;

   DELIMITER $$
 CREATE PROCEDURE SP_Autenticar(IN nick1 VARCHAR(255), IN pass1 VARCHAR(255))
	BEGIN
		SELECT * FROM Usuario WHERE nick = nick1 AND pass = pass1;
    END $$
 DELIMITER ;

 
 
 
